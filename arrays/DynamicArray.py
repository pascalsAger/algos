import ctypes

class DynamicArray(object):
    
    def __init__(self):
        self.count = 0
        self.size = 1
        self.array = self.make_array(self.size)
        
    def make_array(self, size):
        return (size * ctypes.py_object)()
    
    def _resize(self, size):
        
        tempArray = self.make_array(size)
        
        for k in range(self.count):
            tempArray[k] = self.array[k]
            
        self.array =  tempArray
        self.size = size
    
    def __len__(self):
        return self.count
    
    def __getitem__(self, idx):
        if not 0 <= idx < self.count:
            return IndexError('idx is out of bounds!')
        
        return self.array[idx]
    
    def append(self, element):
        if self.count == self.size:
            self._resize(2*self.size)
            
        self.array[self.count] = element
        self.count += 1