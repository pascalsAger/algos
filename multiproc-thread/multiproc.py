from multiprocessing import Process
import time

def cal_sq(num,results_sq):
    print("Calculating Square")
    global results_sq
    for n in num:
        time.sleep(0.2)
        print("Square: ",n*n)
        results_sq.append(n*n)
        
def cal_cube(num):
    print("Calculating Cube")
    for n in num:
        time.sleep(0.2)
        print("Cube: ",n*n*n)        

if __name__=='__main__':
    arr=[2,3,4,5]
    results_sq = []
    p1=Process(target=cal_sq,args=(arr,results_sq))
    p2=Process(target=cal_cube,args=(arr,))
    #cProfile.run('re.compile("foo|bar")')
    t1=time.time()
    p1.start()
    p2.start()
    p1.join()
    p2.join()
    t2=time.time()
    print('Time take is {} seconds'.format(t2-t1))
    print(results_sq)
