from multiprocessing import Process, Queue, current_process

def sort_worker(tasks,results):
	task=tasks.get()
	sorted_list=sorted(task)
	results.put(sorted_list)

if __name__=='__main__':
	n=10
	myTasks=Queue()
	myResults=Queue()

	Workers = [ Process(target=sort_worker, args=(myTasks,myResults)) for i in range(n)]

	for each in Workers:
		each.start()

	for each in range(n):
		myTasks.put([13,8,5,72])


	while n:
		result = myResults.get()
		print(result)
		n-=1
