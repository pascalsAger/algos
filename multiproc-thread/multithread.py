
import threading
import time


def cal_sq(num):
    print("Calculating Square")
    
    for n in num:
        time.sleep(0.2)
        print("Square: ",n*n)
        results_sq.append(n*n)
        
def cal_cube(num):
    print("Calculating Cube")
    for n in num:
        time.sleep(0.2)
        print("Cube: ",n*n*n)        

if __name__=='__main__':
    results_sq = []
    arr=[2,3,4,5]
    p1=threading.Thread(target=cal_sq,args=(arr,))
    p2=threading.Thread(target=cal_cube,args=(arr,))
    #cProfile.run('re.compile("foo|bar")')
    t1=time.time()
    p1.start()
    p2.start()
    p1.join()
    p2.join()
    t2=time.time()
    print('Time take is {} seconds'.format(t2-t1))
    print(results_sq)
