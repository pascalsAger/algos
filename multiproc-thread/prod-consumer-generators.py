'''
producer consumer model with single producer and single consumer using python generators
'''


import random

def get_data():
	return random.sample(range(10),3)


def consume():
	while True:
		data = yield
		print("Consumed " + str(data))


def produce():
	while True:
		data=get_data()
		print("Produced " + str(data))
		consumer.send(data)
		yield

if __name__=='__main__':
	consumer = consume()
	consumer.send(None)
	producer = produce()

	for _ in range(int(input("Enter no. of cycles "))):
		print("Starting communication model")
		next(producer)