import random
import time

L = list(range(1, 10000000))
random.shuffle(L)  # shuffles in-place
#print(L)

if __name__ == '__main__':
	t1=time.time()
	L.sort()
	t2=time.time()
	print('Time take is {} seconds'.format(t2-t1))
	#print(L)

